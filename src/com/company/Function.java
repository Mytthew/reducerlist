package com.company;

public interface Function<T> {
	T apply(T arg1, T arg2);
}
