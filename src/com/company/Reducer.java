package com.company;

import java.util.Arrays;
import java.util.List;

public class Reducer {
	public static <E> E reduce(List<E> list, Function<E> f, E initialValue) {
		E result = initialValue;
		for (int i = 0; i < list.size(); i++) {
			result = f.apply(result, list.get(i));
		}
		return result;
	}

    public static void main(String[] args) {
		Function<Integer> sum = Integer::sum;
		Function<Integer> sumOfSquares = (arg1, arg2) -> arg1 + arg2 * arg2;
		Function<Integer> sumOfCubics = (arg1, arg2) -> arg1 + arg2 * arg2 * arg2;

		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
		System.out.println(reduce(list, sum, 0));
		System.out.println(reduce(list, sumOfSquares, 0));
		System.out.println(reduce(list, sumOfCubics, 0));
    }
}
